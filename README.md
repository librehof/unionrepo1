## Unionrepo 1

Apache 2.0 (C) 2021 [librehof.com](http://librehof.com)


## Summary

- The unionrepo+unitrepo(s) makes up a **lightweight** deployment solution
- This repo serves as a template for a **union** of unit repos
- Makes direct use of git for deployment/update (no separate packaging needed)
- Is dependent on ssh + git + POSIX shell for initial setup
- For more details see **unitrepo** (at [librehof.com](http://librehof.com))
- [Legal notice](NOTICE)


### Get

```
# download
git clone --depth 10 https://gitlab.com/librehof/unionrepo1.git
cd unionrepo1

# update
cd unionrepo1
git pull --depth 10
 
# remove
rm -rf unionrepo1
```


### Adopt

To make your repo a **union** repo:
```
NEWREPO="<your repo>"

# make new git repo
mkdir -p "${NEWREPO}"
cd "${NEWREPO}"
git init
<create remote repo>
git remote add origin <url of your new repo>
git push -u origin <name of your default branch>
cd ..

# clone this repo (if needed)
git clone --depth 10 https://gitlab.com/librehof/unionrepo1.git

cp "unionrepo1/union" "${NEWREPO}/"
cp "unionrepo1/superlib" "${NEWREPO}/"
cp "unionrepo1/gitignore" "${NEWREPO}/.gitignore"

mkdir -p "${NEWREPO}/config"

cd "${NEWREPO}"
./union state
git add .
git commit -m "initial"
git push
```


## Initial setup on one machine

```
# if remote, then setup ssh with appropriate login method, then
ssh <machine>

apt-get install git
or: apk add git 
or: pacman -S git 
or: yum install git 
or: dnf install git 
or: brew install git 

# on the git server, add the machine's ssh key as a "deploy key"
ls ~/.ssh/*.pub

# to create new EdDSA key: ssh-keygen -t ed25519 -a 100
# or for a traditional RSA key: ssh-keygen -t rsa -b 4096

echo ${HOSTNAME} # verify hostname

git clone -b ${HOSTNAME} <uniongiturl> <uniondir>

cd <uniondir>
git branch # verify branch
./union setup
```


## Update on one machine

```
# ssh <machine>
cd <uniondir>
git pull # if you want to refresh config (while running)
./union update
```
if you want to stop before refreshing config:
```
# ssh <machine>
cd <uniondir>
./union teardown
git pull
./union update
./union setup
```


## Usecase 1 

Union repo describing the setup of a **distributed system**

- Each branch = one conmplete working environment
  - prod
  - test
  - etc.
- For the **prod** branch we have two host machines (1 and 2)
- **prod** branch example
  - config/
    - unit1/
      - host.var = hostname of host machine 1
      - \<other config\>
    - unit2/
      -  host.var = hostname of host machine 2
      - \<other config\>
    - unit3/
      -  host.var = hostname of host machine 2
      - \<other config\>
  - union - union script to invoke on update / setup / teardown
  
The repo is cloned into both machine 1 and 2

On both machines we run: `./union update`

- On machine 1, unit1 will be setup
- On machine 2, unit2 and unit3 will be setup


## Usecase 2

Union repo describing the setup of **two independent machines** you work on

- Each branch = machine's hostname
  - mylaptop1
  - mylaptop2
  - etc.
- **mylaptop1** branch
  - config/
    - unit1/
      - \<config for unit1 at mylaptop1\>
    - unit2/
      - \<config for unit2 at mylaptop1\>
  - union - union script to invoke on update / setup / teardown
- **mylaptop2** branch ...

The repo is cloned into each machine (branch = machine's hostname):  


## Union commands
```
./union setup [deps]
./union teardown [all]

./union update [offline]
./union fetch

./union stop
./union release

./union state # unready|bound|active
./union states
```


## Special files

- config/\<unit\>/update.var
  - if `skip` then update will be skipped for this unit
  - if `offline` then update will be made offline for this unit
- config/\<unit\>/host.var
  - if exists and hostname does not match then unit will be skipped
  